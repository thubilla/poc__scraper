# Scrape Content

-  [http://www.gastrojournal.org/article/0016-5085(94)90398-0/fulltext](http://www.gastrojournal.org/article/0016-5085(94)90398-0/fulltext) 
-  [http://www.gastrojournal.org/article/S0016-5085(14)00220-0/fulltext](http://www.gastrojournal.org/article/S0016-5085(14)00220-0/fulltext)
-  [https://mydigitalpublication.com/publication/?i=464113#{"issue_id":464113,"page":0}](http://www.gastrojournal.org/article/S0016-5085(14)00220-0/fulltext)
-  [https://mydigitalpublication.com/publication/?i=464115#{"issue_id":464115,"page":0}](https://mydigitalpublication.com/publication/?i=464115#{"issue_id":464115,"page":0})
-  [http://035bf57.netsolhost.com/AGA-MLU/mlu_medication.php](http://035bf57.netsolhost.com/AGA-MLU/mlu_medication.php)
-  [http://035bf57.netsolhost.com/AGA-MLU/mlu_biologics.php](http://035bf57.netsolhost.com/AGA-MLU/mlu_biologics.php)

### mydigitalpublication.com

- skip

### gastrojournal.org

- Grab abstract text and references list
- `document.querySelector('.fullText').innerHTML`

## netsolhost.com

- Grab Title: `document.querySelector('h2.grayblue').textContent`
- Grab `<p>` Content & `<ul>` Content: `document.querySelector('.span_3_of_4').innerHTML`
- Grab Read More List Content: `document.querySelector('.read-more-content').innerHTML`
