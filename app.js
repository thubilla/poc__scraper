const puppeteer = require('puppeteer');
const fs = require('fs');

const sources = fs.readFileSync('./sources.json');

async function scrape(){
	var src = JSON.parse(sources);
	for(let [key, url] of Object.entries(src)){
			
		try {
			let browser = await puppeteer.launch({
				headless: false
			});
			
			let page = await browser.newPage();
			
			await page.goto(url);
			await page.waitFor(5000);		
			
			let output;
			let fileName = `./output/${key}.html`;

			if(key === '66'){
				output = await page.evaluate(function(){
					let abstract = document.querySelector('.abstract').innerHTML;
					let references = document.querySelector('.referenceList').innerHTML;
					return `<div>
						${abstract}
						${references}
					</div>`;
				});

			} else if(key === '67'){
				let abstract = await page.evaluate(function(){
					return document.querySelector('.abstract').innerHTML;
				});
				console.log('got abstract');
				await page.click('a.showReferences');
				await page.waitFor(2000);
				
				let references = await page.evaluate(function(){
					return document.querySelector('.referenceList').innerHTML;
				});
				console.log('got references');	
				output = `<div>${abstract} ${references}</div>`; 
			} else if(key === '70' || key === '71'){
				output = await page.evaluate(function(){
					let title = document.querySelector('h2.grayblue').outerHTML;
					let summary = document.querySelector('.span_3_of_4').querySelector('p').outerHTML;
					let list = document.querySelector('.span_3_of_4').querySelector('ul').outerHTML;
					let readMore = document.querySelector('.read-more-content').outerHTML;

					return `<div>
						${title}
						${summary}
						${list}
						${readMore}
					</div>`;
				});	
			}

			fs.writeFile(fileName, output, function(err, data){
				if(err) throw err;
				console.log(`Wrote ${fileName}`);
			});
			
			browser.close();
		} catch(err){
			console.error(err);
			throw err;
		}
	}
}

scrape();
